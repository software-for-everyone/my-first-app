<?php

class HttpRequest extends CI_Model {

    private $host = 'https://sistema.ipm.org.gt/rest';
    public function get($array)
    {
        $token = (isset($_SESSION['token'])) ? $_SESSION['token'] : '';
        $correo = (isset($_SESSION['correo'])) ? $_SESSION['correo'] : '';
        $headers  = array(
            'http' => array (
                'method' => 'GET'
                ,'header'=> "Content-Type: application/json\r\n"
                    ."token: $token\r\n"
                    ."correo: $correo\r\n"
            )
        );
        $context = stream_context_create($headers);
        $resultado = file_get_contents($this->host.$array['url'],false,$context);
        if($resultado){
            $response = json_decode($resultado,true);
        }else{
            $response['mensaje'] = 'Sin conexión';
        }
        return $response;
    }

    public function post($array)
    {
        $postData = ($array['datos']) ? json_encode($array['datos']) : '';
        $token = (isset($_SESSION['token'])) ? $_SESSION['token'] : '';
        $correo = (isset($_SESSION['correo'])) ? $_SESSION['correo'] : '';
        $headers  = array(
            'http' => array (
                'method' => 'POST'
                ,'header'=> "Content-Type: application/json\r\n"
                    ."token: $token\r\n"
                    ."correo: $correo\r\n"
                ,'content' => $postData 
            )
        );
        $context = stream_context_create($headers);
        $resultado = file_get_contents($this->host.$array['url'],false,$context);
    
        if($resultado){
            $response = json_decode($resultado,true);
        }else{
            $response['mensaje'] = 'Sin conexión';
        }
        return $response;
    }

    public function put($array)
    {
        $postData = ($array['datos']) ? json_encode($array['datos']) : '';
        $token = (isset($_SESSION['token'])) ? $_SESSION['token'] : '';
        $correo = (isset($_SESSION['correo'])) ? $_SESSION['correo'] : '';
        $headers  = array(
            'http' => array (
                'method' => 'PUT'
                ,'header'=> "Content-Type: application/json\r\n"
                    ."token: $token\r\n"
                    ."correo: $correo\r\n"
                ,'content' => $postData 
            )
        );
        $context = stream_context_create($headers);
        $resultado = file_get_contents($this->host.$array['url'],false,$context);
    
        if($resultado){
            $response = json_decode($resultado,true);
        }else{
            $response['mensaje'] = 'Sin conexión';
        }
        return $response;
    }

    public function delete($array)
    {
        $postData = ($array['datos']) ? json_encode($array['datos']) : '';
        $token = (isset($_SESSION['token'])) ? $_SESSION['token'] : '';
        $correo = (isset($_SESSION['correo'])) ? $_SESSION['correo'] : '';
        $headers  = array(
            'http' => array (
                'method' => 'DELETE'
                ,'header'=> "Content-Type: application/json\r\n"
                    ."token: $token\r\n"
                    ."correo: $correo\r\n"
                ,'content' => $postData 
            )
        );
        $context = stream_context_create($headers);
        $resultado = file_get_contents($this->host.$array['url'],false,$context);
    
        if($resultado){
            $response = json_decode($resultado,true);
        }else{
            $response['mensaje'] = 'Sin conexión';
        }
        return $response;
    }

    public function patch($array)
    {
        $postData = ($array['datos']) ? json_encode($array['datos']) : '';
        $token = (isset($_SESSION['token'])) ? $_SESSION['token'] : '';
        $correo = (isset($_SESSION['correo'])) ? $_SESSION['correo'] : '';
        $headers  = array(
            'http' => array (
                'method' => 'PATCH'
                ,'header'=> "Content-Type: application/json\r\n"
                    ."token: $token\r\n"
                    ."correo: $correo\r\n"
                ,'content' => $postData 
            )
        );
        $context = stream_context_create($headers);
        $resultado = file_get_contents($this->host.$array['url'],false,$context);
    
        if($resultado){
            $response = json_decode($resultado,true);
        }else{
            $response['mensaje'] = 'Sin conexión';
        }
        return $response;
    }
}