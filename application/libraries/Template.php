<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Template {
    
    private $titulo;
	public function __construct() {
        $this->CI =& get_instance();
	}

	public function getTemplate($titulo){
        $data['titulo'] = $titulo;
        return array(
            'header'=> $this->CI->load->view('templates/header.php',$data,TRUE)
            ,'nav'=>$this->CI->load->view('templates/nav.php',$data,TRUE)
            ,'aside'=>$this->CI->load->view('templates/aside.php',$data,TRUE)
            ,'footer'=>$this->CI->load->view('templates/footer.php','',TRUE)
        );
	}
}