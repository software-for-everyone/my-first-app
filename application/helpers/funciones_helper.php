<?php
function fecha($fecha, $separador='/', $divisor="-") {
    if(empty($fecha)){
        return "";
    }
    $tmp=explode("$divisor",$fecha);
    return $tmp[2].$separador.$tmp[1].$separador.$tmp[0];
}

function moneda($valor='0',$simbolo="") {
    if(empty($valor)){$valor=0;}
    return $simbolo.number_format($valor,2, ".",",");
}

function estado($numero_estado=''){
    if($numero_estado){
        return '<span class="label label-success"><span class="icon mdi mdi-thumb-up"></span> Activo</span>';
    }else{
        if($numero_estado==='0'){
            return '<span class="label label-danger"><span class="icon mdi mdi-thumb-down"></span> Inactivo</span>';
        }else{
            return '<span class="label label-warning">Pendiente</span>';
        }
    }
}

function accionesButton($uri,$id) {
    return '
    <a class="btn btn-warning waves-effect" href="'.$uri.'/registro/'.$id.'" >
        <i class="material-icons">edit</i> Editar
    </a>
    <a class="btn btn-danger waves-effect" href="'.$uri.'/eliminar/'.$id.'" >
        <i class="material-icons">delete</i> Eliminar
    </a>
    <!--<div class="btn-group">
        <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Estado 
            <i class="material-icons">expand_more</i>
        </button>
        <ul role="menu" class="dropdown-menu ">
            <li>
                <a href="'.$uri.'/activar/'.$id.'" ><span class="material-icons">thumb_up</span> Activo</a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="'.$uri.'/inactivar/'.$id.'" ><span class="material-icons">thumb_down</span> Inactivo</a>
            </li>
        </ul>
    </div>-->';
}

function accionesButtonModificar($uri,$id) {
    return '
    <a class="btn btn-warning waves-effect" href="'.base_url($uri.'/modificar/'.$id).'" >
        <i class="mdi mdi-edit"></i> Editar
    </a>';
}

function accionesButtonCrear($url) {
    return '
    <a class="btn btn-success btn-lg" href="'.base_url($url.'/nuevo').'" >
        <i class="material-icons">add</i> Nuevo registro
    </a>';
}

function accionesButtonEstado($uri,$id) {
    return '
    <div class="btn-group btn-hspace ">
        <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle btn-rounded ">Cambiar estado <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
        <ul role="menu" class="dropdown-menu ">
            <li>
                <a href="'.base_url($uri.'/activar/'.$id).'" ><span class="icon mdi mdi-thumb-up"></span> Activo</a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="'.base_url($uri.'/inactivar/'.$id).'" ><span class="icon mdi mdi-thumb-down"></span> Inactivo</a>
            </li>
        </ul>
    </div>';
}

function buttonNuevoRegistro($uri)
{
    return '
    <a class="btn btn-success btn-lg" href="'.$uri.'/registro/" >
        <i class="material-icons">add</i> Nuevo registro
    </a>';
}

function selectForm($id,$nombre,$url)
{
    $response = callApi($url,'GET','');   
    $menu = '
    <div class="form-group">
        <label name="'.$id.'" class="col-sm-3 control-label">'.$nombre.'</label>
        <div class="col-sm-6">
            <select class="form-control">
            <option>--Elija una opción</option>';
    if(!isset($response['mensaje'])){
        foreach ($response as $numero => $infoMenu){
            $menu .= '<option value="'.$infoMenu['id'].'";>'.$infoMenu['nombre'].'</option>';
        }
    }
    $menu .= '</select>
        </div>
    </div>';
    
    return $menu;
}

function url_imagen_usuario($imagen_path){
    if($imagen_path){
        $imagen_usuario = 'assets/img/usuario/'.$imagen_path;
        $imagen_empresa = 'assets/img/empresa/'.$imagen_path;
		if(is_file($imagen_usuario)){
            $imagen = base_url($imagen_usuario);
        }else{
            $imagen = base_url($imagen_empresa);
        }
    }else{
        $imagen = base_url('assets/img/user.png');
    }
    return $imagen;
}


