<?php
function getReglasLogin(){
    return $config = array(
        array(
            'field' => 'usuario',
            'label' => 'usuario',
            'rules' => 'required',
            'errors' => array(
                'required' => 'Debes de ingresar un %s',
            ),
        ),
        array(
            'field' => 'clave',
            'label' => 'contraseña',
            'rules' => 'required',
            'errors' => array(
                'required' => 'Debes de ingresar una %s.',
            ),
        )
    );
}

?>