<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('auth/reglas_login');
	}
	public function index()
	{
		$this->form_validation->set_error_delimiters('', '');
		$reglas = getReglasLogin();	
		$data['mensaje'] = '';
		$this->form_validation->set_rules($reglas);
		if ($this->form_validation->run() == FALSE) {
			$errors = array(
				'usuario' => form_error('usuario'),
				'clave' => form_error('clave')
			);
			$this->load->view('auth/login');
		} else {
			$request['url'] = '/api/login';
			$request['datos'] = $_POST;
			$response = $this->HttpRequest->post($request);
			if(isset($response['usuario'])){
				$datos_session = array(
					"usuario" => $response['usuario']
					,"nombre" => $response['nombre']
					,"apellido" => $response['apellido']
					,"correo" => $response['correo']
					//,"imagen" => $response['imagen']
					,"cod_depto" => $response['cod_depto']
					,"departamento" => $response['departamento']
					//,"token" => $response['token']
					,"is_logged" => TRUE
				);
				$this->session->set_userdata($datos_session);
				redirect( base_url('inicio'), 200);
			}else{
				$data['mensaje'] = $response['mensaje'];
				$this->load->view('auth/login',$data);
			}
		}
	}

	public function logout()
	{
		$datos_session = array(
			"usuario"
			,"nombre"
			,"apellido"
			,"correo"
			//,"imagen"
			,"cod_depto"
			,"departamento"
			//,"token"
			,"is_logged"
		);
		$this->session->unset_userdata($datos_session);
		$this->session->sess_destroy();
		redirect(base_url('login'), 200);
	}

}
