<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}
	
	public function index()
	{
		$data = $this->template->getTemplate('TICKETS');
		$request['url'] = '/api/tipoTicket';
		$response = $this->HttpRequest->get($request);
		$data['response'] = $response;	
		$this->load->view('todos/tickets/ticket_list',$data);
	}

	public function registro()
	{
		$data = $this->template->getTemplate('SOLICITUD DE TICKET');
		$this->load->view('todos/tickets/ticket_form',$data);
	}
}
