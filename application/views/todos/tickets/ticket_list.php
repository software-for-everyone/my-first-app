<?=$header?>
<?=$nav?>
<?=$aside?>
<!-- CONTENIDO -->
<?php $uri = $this->uri->segment(1);?>
<link href="<?=base_url('theme/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')?>" rel="stylesheet">
<section class="content">
    <div class="content-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-light-blue-dos">
                        <h2>
                            <?=(isset($titulo))? $titulo:''?>
                        </h2>
                    </div>
                    <div class="body">
                        <br>
                        <?=buttonNuevoRegistro($uri);?>
                        <br>
                        <br>
                        <?php if(!isset($response['mensaje'])):?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Descripcion</th>
                                        <th>Usuario</th>
                                        <th>Fecha</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $cont=0;foreach ($response as $id => $datos):
                                    //if ($datos['id']>6) break;?>
                                    <tr>
                                        <td><?=$datos['id']?></td>
                                        <td><?=$datos['descripcion']?></td>
                                        <td><?=$datos['usuario']?></td>
                                        <td><?=$datos['fecha']?></td>
                                        <td><?=accionesButton($uri,$datos['id']);?></td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <?php else:?>
                        <br><br>
                        <div class="alert alert-warning">
                            <strong><?=$response['mensaje']?>!</strong>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Input -->
    </div>
</section>
<?=$footer?>