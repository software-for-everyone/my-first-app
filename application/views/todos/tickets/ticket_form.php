<?=$header?>
<?=$nav?>
<?=$aside?>
<!-- CONTENIDO -->
<?php $uri = $this->uri->segment(1);?>
<section class="content">
    <div class="content-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-light-blue-dos">
                        <h2>
                            <?=(isset($titulo))? $titulo:''?>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <form action="" method="post">
                                <div class="col-sm-12">
                                    <br>
                                    <div class="form-group form-float form-group-lg">
                                        <label class="form-label">Descripción</label>
                                        <div class="form-line">
                                            <textarea rows="2" class="form-control no-resize" placeholder="Describa el problema"></textarea>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-success btn-lg waves-effect">
                                        <i class="material-icons">done</i>
                                        ENVIAR TICKET
                                    </button>
                                    <a class="btn btn-default btn-lg waves-effect" href="<?=base_url("$uri")?>">
                                        <i class="material-icons">close</i>
                                        CANCELAR
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Input -->
    </div>
</section>
<?=$footer?>