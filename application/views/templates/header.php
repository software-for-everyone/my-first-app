<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*if(!empty($_SESSION['id'])){
  $request['url'] = '/api/token/'.$_SESSION['id'];
  $tokenActivo = $this->HttpRequest->get($request);
  if(!$tokenActivo['token']){
    redirect( base_url('login'), 200);
  }
}else{
  redirect( base_url('login'), 200);
}*/
if(empty($_SESSION['usuario'])){
    redirect( base_url('login'), 200);
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Nuevo proyecto</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('dist/css/all.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url('dist/css/ionicons.min.css')?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=base_url('dist/css/adminlte.min.css')?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="<?=base_url('dist/css/fonts.api.css')?>" rel="stylesheet">
  <style>
    .salir:hover {color: red;}
    .salir:active {color: white;}
  </style>
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">